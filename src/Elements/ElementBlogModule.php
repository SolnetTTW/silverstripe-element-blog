<?php

namespace Solnet\Elements;

use \Page;
use Silvershop\HasOneField\HasOneButtonField;
use SilverStripe\Blog\Model\Blog;
use SilverStripe\Blog\Model\BlogCategory;
use SilverStripe\Blog\Model\BlogPost;
use SilverStripe\Blog\Model\BlogTag;
use SilverStripe\Control\Controller;
use SilverStripe\Core\Convert;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\View\Requirements;
use SilverStripe\Versioned\Versioned;

class ElementBlogModule extends CustomBaseElement
{
    private static $table_name = 'ElementBlogModule';
    private static $singular_name = 'Blog Content';
    private static $plural_name = 'Blog Content';
    private static $description = 'Blog Content Module';

    private static $icon = 'font-icon-block-content';

    private static $db = [
        'Heading' => 'HTMLText',
        'MaxColumns' => 'Int',
        'AllSubsites' => 'Boolean',
        'UserFiltersEnabled' => 'Boolean'
    ];

    private static $has_one = [
        'Blog' => Blog::class,
        'BlogCategory' => BlogCategory::class,
        'BlogTag' => BlogTag::class,
    ];

    /**
     * Name of the block as displayed in the CMS.
     *
     * @return String
     */
    public function getType()
    {
        return _t('Element.SolnetBlog_BlockType', 'Blog Content');
    }

    public function getCMSFields()
    {
        Requirements::javascript('solnet/silverstripe-element-blog:client/dist/js/ElementBlogCMS.js');

        $fields = parent::getCMSFields();
        $fields->removeByName(array('AllSubsites', 'BlogID', 'BlogTagID', 'BlogCategoryID', 'BlogPosts', 'BackgroundID', 'UserFiltersEnabled'));

        if (!$this->exists()) {
            // Has not been saved yet
            $fields->addFieldToTab(
                'Root.Main',
                LiteralField::create(
                    'SavingTip',
                    _t('Element.SolnetBlog_SavingTip', '<p class="message warning">Please save to see more options.</p>')
                )
            );
        } else {
            $fields->addFieldsToTab(
                'Root.Main',
                [
                    DropdownField::create(
                        'MaxColumns',
                        _t('Element.SolnetBlog_MaxColumns_Title', 'Maximum number of Blog posts to display'),
                        ['Unlimited', 1, 2, 3, 4]
                    ),
                    $blogID = DropdownField::create(
                        'BlogID',
                        _t('Element.SolnetBlog_Blog_Title', 'Blog'),
                        Blog::get()->map()
                    ),
                    $blogCategoryID = DropdownField::create(
                        'BlogCategoryID',
                        _t('Element.SolnetBlog_BlogCategory_Title', 'Blog Category'),
                        BlogCategory::get()->map()
                    ),
                    $blogTagID = DropdownField::create(
                        'BlogTagID',
                        _t('Element.SolnetBlog_BlogTag_Title', 'Blog Tag'),
                        BlogTag::get()->map()
                    ),
                ]
            );

            $blogID->setEmptyString(_t('Element.SolnetBlog_Blog_EmptyString', 'Any blog'));
            $blogCategoryID->setEmptyString(_t('Element.SolnetBlog_BlogCategory_EmptyString', 'Do not filter'));
            $blogTagID->setEmptyString(_t('Element.SolnetBlog_BlogTag_EmptyString', 'Do not filter'));

            // Subsites module adds an extra field
            if (class_exists('SilverStripe\Subsites\Model\Subsite')) {
                $fields->insertAfter(
                    'BlogID',
                    $allSubsites = CheckboxField::create(
                        'AllSubsites',
                        _t('Element.SolnetBlog_AllSubsites_Title', 'Include posts from main site and all subsites?')
                    )
                );
                $allSubsites->setDescription(
                    _t('Element.SolnetBlog_AllSubsites_Description', 'Only applies if "Blog" is set to "Any blog".')
                );
            }

            $fields->insertAfter(
                'AllSubsites',
                $userFiltersEnabled = CheckboxField::create(
                    'UserFiltersEnabled',
                    _t('Element.SolnetBlog_UserFiltersEnabled_Title', 'Enable user filters for blog posts?')
                )
            );
        }

        return $fields;
    }

    /**
     * Returns blog posts as they should be displayed within the Silverstripe rendered template.
     * @return DataList BlogPost items
     */
    public function getTemplateBlogPosts()
    {
        if ($this->BlogID) {
            $posts = $this->Blog()->getBlogPosts();
        } else {
            if (class_exists('SilverStripe\Subsites\Model\Subsite') && $this->AllSubsites) {
                $posts = \SilverStripe\Subsites\Model\Subsite::get_from_all_subsites(BlogPost::class);
            } else {
                $posts = BlogPost::get();
            }
        }

        if ($this->BlogCategoryID) {
            $stage = Versioned::get_stage();
            if ($stage == 'Stage') {
                $stage = '';
            } elseif ($stage) {
                $stage = '_' . $stage;
            }

            $posts = $posts
            ->leftJoin("BlogPost_Categories", '"BlogPost_Categories"."BlogPostID" = "BlogPost' . $stage . '"."ID"')
            ->where('"BlogPost_Categories"."BlogCategoryID" = ' . $this->BlogCategoryID);
        }

        if ($this->BlogTagID) {
            $stage = Versioned::get_stage();
            if ($stage == 'Stage') {
                $stage = '';
            } elseif ($stage) {
                $stage = '_' . $stage;
            }

            $posts = $posts
            ->leftJoin("BlogPost_Tags", '"BlogPost_Tags"."BlogPostID" = "BlogPost' . $stage . '"."ID"')
            ->where('"BlogPost_Tags"."BlogTagID" = ' . $this->BlogTagID);
        }

        return $posts->sort('PublishDate DESC')->limit($this->MaxColumns);
    }


    /**
     * @param $tags Array of tag IDs to filter by
     * @param $categories Array of category IDs to filter by
     * @return DataList BlogPost items
     */
    public function getBlogPosts($tags = [], $categories = [])
    {
        if ($this->BlogID) {
            $posts = $this->Blog()->getBlogPosts();
        } else {
            if (class_exists('SilverStripe\Subsites\Model\Subsite') && $this->AllSubsites) {
                $posts = \SilverStripe\Subsites\Model\Subsite::get_from_all_subsites(BlogPost::class);
            } else {
                $posts = BlogPost::get();
            }
        }

        if (count($categories) > 0) {
            $stage = Versioned::get_stage();
            if ($stage == 'Stage') {
                $stage = '';
            } elseif ($stage) {
                $stage = '_' . $stage;
            }
            $posts = $posts->leftJoin(
                "BlogPost_Categories",
                '"BlogPost_Categories"."BlogPostID" = "BlogPost' . $stage . '"."ID"'
            );

            $where = [];
            foreach ($categories as $catID) {
                $where[] = '"BlogPost_Categories"."BlogCategoryID" = ' . $catID;
            }
            $posts = $posts->where(implode(' OR ', $where));
        }

        if (count($tags) > 0) {
            $stage = Versioned::get_stage();
            if ($stage == 'Stage') {
                $stage = '';
            } elseif ($stage) {
                $stage = '_' . $stage;
            }
            $posts = $posts->leftJoin(
                "BlogPost_Tags",
                '"BlogPost_Tags"."BlogPostID" = "BlogPost' . $stage . '"."ID"'
            );

            $where = [];
            foreach ($tags as $tagID) {
                $where[] = '"BlogPost_Tags"."BlogTagID" = ' . $tagID;
            }
            $posts = $posts->where(implode(' OR ', $where));
        }

        return $posts->sort('PublishDate DESC');
    }

    /**
     * Returns a string describing the data for this element, as JSON. Used to render the page initially, and also
     * used to output API calls for new data.
     *
     * If an HTTP request object is supplied, uses this to select tags and categories.
     *
     * If no HTTP request object is supplied, falls back to using the defaults specified.
     *
     * @param $request HTTPRequest object to use
     * @return string
     */
    public function getJsonData($request = null)
    {
        // Default data
        $data = [
            // API endpoint to fetch new content from
            'endpoint' => Controller::join_links('ElementBlogModule', 'data', $this->ID),
            // The posts to display
            'posts' => '',
            // Available tags and categories - associative array ID => Name
            'tags' => [],
            'categories' => [],
            // Selected tags and categories - array of IDs
            'selectedTags' => [],
            'selectedCategories' => [],
        ];

        // Find the selected tags and categories; if not from a GET param then
        // fall back to the configured values
        $data['selectedTags'] = [];
        if ($this->BlogTagID) {
            $data['selectedTags'] = ["$this->BlogTagID"];
        }
        if ($request !== null) {
            $data['selectedTags'] = [];
            if (is_array($request->getVar('tags'))) {
                foreach ($request->getVar('tags') as $tagID) {
                    $tag = BlogTag::get()->byID($tagID);
                    if (!$tag) {
                        continue;
                    }
                    $data['selectedTags'][] = "$tag->ID";
                }
            }
        }

        $data['selectedCategories'] = [];
        if ($this->BlogCategoryID) {
            $data['selectedCategories'] = ["$this->BlogCategoryID"];
        }
        if ($request !== null) {
            if (is_array($request->getVar('categories'))) {
                $data['selectedCategories'] = [];
                foreach ($request->getVar('categories') as $catID) {
                    $cat = BlogCategory::get()->byID($catID);
                    if (!$cat) {
                        continue;
                    }
                    $data['selectedCategories'][] = "$cat->ID";
                }
            }
        }

        // Get all posts
        $posts = $this->getBlogPosts(
            $data['selectedTags'],
            $data['selectedCategories']
        );
        $maxColumns = (int)$this->MaxColumns;

        // Render post HTML to output, categories, and blogs
        // from full dataset
        foreach ($posts as $i => $post) {
            // Merge categories & tags
            $data['categories'] += $post->Categories()->map('ID', 'Title')->toArray();
            $data['tags'] += $post->Tags()->map('ID', 'Title')->toArray();
            // Only add HTML for the first n posts
            if ($maxColumns <= 0 || $i < $maxColumns) {
                $data['posts'] = $data['posts'] . trim(
                    $post->customise([
                        'Element' => $this,
                    ])
                    ->renderWith('BlogPostCard')
                    ->RAW()
                );
            }
        }

        return Convert::array2json($data);
    }
}
