<?php

namespace Solnet\Elements;

use SilverStripe\Control\Director;
use SilverStripe\Control\Controller;
use SilverStripe\ErrorPage\ErrorPage;

class ElementBlogModuleController extends Controller
{
    private static $url_segment = 'ElementBlogModule';

    private static $allowed_actions = [
        'data',
    ];

    /**
     * Returns a JSON representation of the data needed to render the Blog Module
     */
    public function data()
    {
        $element = ElementBlogModule::get()->byID(
            $this->getRequest()->param('ID')
        );

        if (!$element) {
            return ErrorPage::response_for(404);
        }

        $response = $this->getResponse();
        $response->setBody(
            $element->getJsonData($this->getRequest())
        );
        $response->addHeader('Content-Type', 'application/json');

        return $response;
    }
}
