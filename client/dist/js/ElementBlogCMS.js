(function($) {
  $.entwine('ss', function($){
      $('#Form_ItemEditForm_BlogID').entwine({
          onadd:function(){
              this.toggleFields();
          },
          onchange:function(){
              this.toggleFields();
          },
          toggleFields:function(){
              var value = this.val(),
                  target = $('#Form_ItemEditForm_AllSubsites_Holder');
              if (value) {
                target.hide();
              } else {
                target.show();
              }
          }
      });
  });
}(jQuery));
